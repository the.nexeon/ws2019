// WSExam1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"

#include <iostream>
#include <fstream>

#include <string>

#include <openssl/conf.h>
#include <openssl/evp.h> 
#include <openssl/err.h> 
#include <openssl/aes.h>

#include "DBClass.h"

using namespace std;

void bruteforce(const char* filename, char pass_to_find[5]) {
	unsigned char *crypted = new unsigned char[1000]; // зашифрованная строка
	unsigned char *plaintext = new unsigned char[1000]; // дешифрованная строка
	unsigned char *iv = (unsigned char *)"0123456789012345"; // рандомайзер
	int plaintext_len = strlen((char *)plaintext); // длина строки после обработки
	int len = 0; // Длина строки
	int cr_len = 0; // Длина шифра

	fstream in_crypted(filename, ios::binary | ios::in); // файловый поток
	if (in_crypted.is_open()) { // открываем файл
		in_crypted.read((char*)crypted, 1000); // считываем шифр
		cr_len = in_crypted.gcount(); // получаем кол во считанных символов
		in_crypted.close(); // закрываем файл
	}
	else {
		cout << "Не удалось открыть или прочитать файл" << endl;
		return;
	}

	EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new(); // Создание структуры с настройками метода

	char* password = new char[256]; // шифр

	for (int a = 0; a < 10; a++) {
		for (int b = 0; b < 10; b++) {
			for (int c = 0; c < 10; c++) {
				for (int d = 0; d < 10; d++) {

					pass_to_find[0] = a + '0'; // конвертация из int в char
					pass_to_find[1] = b + '0';
					pass_to_find[2] = c + '0';
					pass_to_find[3] = d + '0';
					pass_to_find[4] = '\0';

					sprintf(password, "0000000000000000000000000000%s", pass_to_find);

					EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, (unsigned char*)password, iv); // Инициализация методом AES, ключом и вектором

					EVP_DecryptUpdate(ctx, plaintext, &len, crypted, cr_len); // дешифровка

					plaintext_len = len;
					EVP_DecryptFinal_ex(ctx, (unsigned char*)plaintext + len, &len); // Финальная обработка

					plaintext_len += len;
					plaintext[plaintext_len] = '\0'; // Обозначаем конец строки

					if (plaintext[0] == '{' && plaintext[1] == '\r') {
						cout << "Ключ: " << password << endl << "Данные: " << plaintext << endl; // вывод в консоль
						return;
					}
				}
			}
		}
	}

	EVP_CIPHER_CTX_free(ctx); // Освобождение памяти
}


int main()
{
	setlocale(LC_ALL, "Russian");

	char pass[5] = "\0";
	bruteforce("3_encrypted", pass);

	DBClass db1;

	char* password = new char[256];
	sprintf(password, "0000000000000000000000000000%s", pass);

	db1.load("1_encrypted", password);

	db1.printAll();

	db1.add("Andrey", "Arhipov", "0000 000000");
	cout << db1.find("Arhipov");

	db1.printAll();

	system("pause");
	
    
}

