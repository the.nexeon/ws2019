#pragma once

#include <iostream>
#include <string>

using namespace std;

class DBRecord
{
public:
	DBRecord(); // ����������� �� ��������� ������� ������ ������
	DBRecord(string name, string surname, string passport); // ������� ������ � ������������ ������ �������� � ���������
	DBRecord(const char* c); // ������� ������ �� ������

	void print(); // ������� ������ � �������
	string getSurname(); // �������� ������� �� ������
 
	~DBRecord();

private:
	char *plaintext; // ������
	string name; // ���
	string surname; // �������
	string passport; // �������
};

